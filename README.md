# final-miscosas

# Entrega práctica
## Datos
* Nombre: Belén Suárez Galiñanes
* Titulación: Tecnologías de Telecomunicación
* Video básico (url): https://www.youtube.com/watch?v=ci87ZlzQmpo
* Video parte opcional (url): https://www.youtube.com/watch?v=VxbWRTmCQ2w
* Despliegue (url): http://bsuarez.pythonanywhere.com/

## Cuenta Admin Site
* usuario/contraseña: belen/belen
## Cuentas usuarios
* usuario/contraseña: guille/guille
* usuario/contraseña: ana/ana
* usuario/contraseña: noa/noa
* usuario/contraseña: prueba/prueba

## Resumen parte obligatoria
* Recurso '/': Muestra la página principal en la que tenenemos:
  * Formulario para seleccionar los distintos alimentadores.
  * Formulario de acceso para usuarios.
  * Listado con los 10 ítems más votados.
  * Listado de alimentadores seleccionados anteriormente.
  * Si está autenticado, listado con los últimos 5 ítems votados y un botón para votar positiva o negativamente en cada ítem.

* Recurso '/alimentadores': Muestra la lista de todos los alimentadores que están guardados en la base de datos.

* Recurso '/alimentadores/<id_alimentador>': Muestra el listado de ítems que tiene ese alimentador. También tenemos un botón que indica si el alimentador está selecionado o no, y que permite seleccionarlo o no (en contraposición a como se encuentre) para que aparezca en la página principal. Y por otro lado, tenemos un botón que nos permite actualizar la información del alimentador.

* Recurso '/alimentadores/<id_alimentador>/<id_item>': Muestra la página del ítem con los comentarios que ha recibido el ítem.
Si el usuario está autenticado, aparecerá también un formulario para añadir comentario y dos botones para votar el ítem.

* Recurso '/users': Muestra una lista de los usuarios que están autenticados en la aplicación.

* Recurso '/users/<id_usuario>': Muestra la página del usuario.
Si el usuario está autenticado, en su página podrá modificar su perfil.

* Recurso '/about': Muestra la página de información de la autoría de la práctica y un resumen del funcionamiento.

## Lista partes opcionales
* Nombre parte: Único formulario para los alimentadores.

* Nombre parte: Comentarios en formato XML y JSON.

* Nombre parte: Inclusión de favicon en el sitio.