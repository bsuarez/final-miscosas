
"""Youtube app resource configuration
"""
from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.main_page, name='main'),
    path('logIn', views.logIn, name='logIn'),
    path('logOut', views.logOut, name='logOut'),
    path('register', views.register, name='register'),
    path('users', views.all_users, name='all_users'),
    path('users/<str:name>', views.pag_user, name='pag_user'),
    path('users/miscosas/css/html.css', views.css, name='css'),
    path('about', views.pag_info, name='pag_info'),
    path('miscosas/css/html.css', views.css, name='css'),
    path('addcomment/<str:id>/<str:videoId>', views.addcomment, name='addcomment'),
    path('addvote/<str:id>/<str:videoId>', views.addvote, name='addvote'),
    path('deselect/<str:id>', views.deselect, name='deselect'),
    path('select/<str:id>', views.select, name='select'),
    path('intermediate', views.intermediate, name='intermediate'),
    path('alimentadores', views.todos_alimentadores, name='alimentadores'),
    path('alimentadores/miscosas/css/html.css', views.css, name='css'),
    path('alimentadores/<str:id>', views.pag_alimentador, name='pag_alimentador'),
    path('alimentadores/<str:id>/miscosas/css/html.css', views.css_id, name='css_id'),
    path('alimentadores/<str:id>/<str:videoId>', views.pag_item, name='pag_item'),
    path('comentarios/xml', views.comentarios_xml, name='comentarios_xml'),
    path('comentarios/json', views.comentarios_json, name='comentaios_json'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
