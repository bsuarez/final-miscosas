from django.contrib import admin

# Register your models here.
from .models import Channel, Video, Commentary, Votes, UserProfile, Configuration

admin.site.register(Channel)
admin.site.register(Video)
admin.site.register(Commentary)
admin.site.register(Votes)
admin.site.register(UserProfile)
admin.site.register(Configuration)