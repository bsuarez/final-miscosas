from django import forms
from .models import *


class ProfileForm(forms.ModelForm):
    profile_photo = forms.ImageField()
    class Meta:
        model = UserProfile
        fields = []


