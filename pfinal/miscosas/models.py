from django.db import models
from django.contrib.auth.models import User

class Channel(models.Model):
    channelId = models.CharField(max_length=100, primary_key=True)
    channelTitle = models.CharField(max_length=256, default="")
    channelLink = models.CharField(max_length=256, default="")
    numVideos = models.IntegerField(default=0)
    votesTotal = models.IntegerField(default=0)
    selected = models.BooleanField(default=True)

class Video(models.Model):
    id = models.CharField(max_length=100, primary_key=True)
    title = models.CharField(max_length=256, default="")
    link = models.CharField(max_length=256, default="")
    description = models.CharField(max_length=500, default="")
    positive_votes = models.IntegerField(default=0)
    negative_votes = models.IntegerField(default=0)
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE, default="")

class Commentary(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")
    videoId = models.ForeignKey(Video, on_delete=models.CASCADE, default="")
    texto = models.TextField(max_length=256)
    fecha = models.DateField(auto_now_add=True)

class Votes(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")
    videoId = models.ForeignKey(Video, on_delete=models.CASCADE, default="")
    vote = models.IntegerField(default=0) #1 positivo, 0 no voto, -1 negativo
    fecha = models.DateTimeField(auto_now=True)

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to='images/')

class Configuration(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")
    color = models.CharField(default='#f7efe6', max_length=32)
    size = models.IntegerField(default=15)
