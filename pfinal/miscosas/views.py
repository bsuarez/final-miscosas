"""Views for YouTube app
"""


from django.http import HttpResponse, HttpResponseRedirect
from .models import Channel, Video, Commentary, Votes, UserProfile, Configuration
from xml.sax import make_parser
import urllib.request
from .ytparser import YTHandler
from .wkparser import WKHandler
from django.template.loader import get_template
from django.views.decorators.csrf import csrf_exempt
from operator import itemgetter
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.views.decorators.clickjacking import xframe_options_exempt
from django.contrib.auth.hashers import make_password
from .forms import ProfileForm
from django.shortcuts import render
from django import template
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_protect

register2 = template.Library()

@register2.filter(name='get_item')
def get_item(dictionary, key):
    return dictionary.get(key)

def parse(channelId):
    theParser = make_parser()
    theHandler = YTHandler()
    theParser.setContentHandler(theHandler)

    xmlFile = urllib.request.urlopen('https://www.youtube.com/feeds/videos.xml?channel_id=' + channelId)
    theParser.parse(xmlFile)

    channelDic = theHandler.getDic()
    return channelDic

def parse_wk(channelId):
    theParser = make_parser()
    theHandler = WKHandler()
    theParser.setContentHandler(theHandler)

    xmlFile = urllib.request.urlopen('https://en.wikipedia.org/w/index.php?title=' + channelId +'&action=history&feed=rss')
    theParser.parse(xmlFile)

    channelDic = theHandler.getDic()
    return channelDic

# guardo info del alimentador canal
def loadData(channelDic, channelId):
    channel = Channel.objects.get(channelId=channelId)
    channel.channelTitle = channelDic['channelTitle']
    channel.channelLink = channelDic['channelLink']
    channel.numVideos = channelDic['numVideos']
    channel.save()
    for video in channelDic['videos']:
        try:
            video_already_exists = Video.objects.get(id=video['id'])
            video_already_exists.title = video['title']
            video_already_exists.link = video['link']
            video_already_exists.description = video['description']
            video_already_exists.save()
        except Video.DoesNotExist:
            id = video['id']
            title = video['title']
            link = video['link']
            description = video['description']
            video = Video(id=id, title=title, link=link, description=description, channel=channel)
            video.save()


# lista de videos de + puntuacion a -
def orden_videos(videos):
    dic = {}
    videos_orden = []
    for video in videos:
        idVideo = video.id
        post_votes = video.positive_votes
        negat_votes = video.negative_votes
        votes = post_votes - negat_votes
        dic[idVideo] = votes
        videos_orden = sorted(dic.items(), key=itemgetter(1), reverse=True)

    top_10 = videos_orden[0:10]
    return top_10


# sacar lista de objetos videos a partir de top10 ordenados por numvotos
def lista_videos(top_10):
    videos_mainpage = []
    for item in top_10:
        video = Video.objects.get(id=item[0])
        videos_mainpage.append(video)
    return videos_mainpage


# calcular el total de votos del canal
def total_votes(alimentadores):
    for alimentador in alimentadores:
        items = Video.objects.all().filter(channel=alimentador)
        positives = 0
        negatives = 0
        for item in items:
            positives = positives + item.positive_votes
            negatives = negatives + item.negative_votes
        total = positives - negatives
        alimentador.votesTotal = total
        alimentador.save()


def change_alimentador(id):
    alimentador = Channel.objects.get(channelId=id)
    alimentador.selected = False
    alimentador.save()


# sacar los 5 ultimas fechas del usuario
def last_voted_videos(votos):
    list_video_date = []
    dic = {}
    for voto in votos:
        fecha_voto = voto.fecha
        list_video_date.append(fecha_voto)
        dic[voto.videoId.id] = fecha_voto
        videos_orden = sorted(dic.items(), key=itemgetter(1), reverse=True)

    top5 = videos_orden[0:5]
    return top5

# PÁGINA PRINCIPAL EN FORMATO JSON
def mainJson(request):
    dict = {}
    dict_top10_items = {}
    dict_alimentadores = {}
    dict_top5_items = {}
    if request.method == 'GET':
        videos = Video.objects.all()  # lista de todos los videos que tengo en la bbdd
        top_10 = orden_videos(videos)
        videos_mainpage = lista_videos(top_10)
        alimentadores = Channel.objects.all().filter(selected=True)
        total_votes(alimentadores)
        for video in videos:
            dict_top10_items[video.title] = {
                'link': video.link,
                'numPosVotes': video.positive_votes,
                'numNegVotes': video.negative_votes,
                'enlace': '/alimentadores/' + video.channel.channelId + '/' + video.id,
            }

        for alimentador in alimentadores:
            dict_alimentadores[alimentador.channelTitle] = {
                'link': alimentador.channelLink,
                'numItems': alimentador.numVideos,
                'votosTot': alimentador.votesTotal,
                'enlace': '/alimentadores/' + alimentador.channelId,
            }

        if request.user.is_authenticated:
            username = request.user.username
            user = User.objects.get(username=username)
            votos = Votes.objects.all().filter(user=user)
            last_voted = last_voted_videos(votos)
            list_last_voted = lista_videos(last_voted)

            for video in list_last_voted:
                dict_top5_items[video.title] = {
                    'link': video.link,
                    'numPosVotes': video.positive_votes,
                    'numNegVotes': video.negative_votes,
                    'enlace': '/alimentadores/' + video.channel.channelId + '/' + video.id,
                }

        dict['itemsTop10'] = dict_top10_items
        dict['alimentadores'] = dict_alimentadores
        dict['itemsTop5'] = dict_top5_items

       # print(json.dumps(dict, default=str, indent=4))

        return dict

#PÁGINA PRINCIPAL EN FORMATO XML
def mainXml(request):
    if request.method == "GET":
        videos = Video.objects.all()  #lista de todos los videos que tengo en la bbdd
        top_10 = orden_videos(videos)
        videos_mainpage = lista_videos(top_10)
        alimentadores = Channel.objects.all().filter(selected=True)
        total_votes(alimentadores)
        if request.user.is_authenticated:
            username = request.user.username
            user = User.objects.get(username=username)
            votos = Votes.objects.all().filter(user=user)
            last_voted = last_voted_videos(votos)
            list_last_voted = lista_videos(last_voted)

            context = {'videos': videos_mainpage, 'alimentadores': alimentadores, 'votos': votos,
                           'list_last_voted': list_last_voted}
            #template = get_template('miscosas/plantilla_mainxml.xml')
        else:
            context = {'videos': videos_mainpage, 'alimentadores': alimentadores}
            #template = get_template('miscosas/plantilla_mainxml.xml')

    return context

# PÁGINA PRINCIPAL
@csrf_exempt
def main_page(request):
    if request.method == "GET":
        formato = request.GET.get('format')
        if formato != '':
            if formato == 'xml':
                context = mainXml(request)
                template = get_template('miscosas/plantilla_mainxml.xml')
                return HttpResponse(template.render(context, request), content_type='text/xml')
            elif formato == 'json':
                dict = mainJson(request)
                return JsonResponse(dict)
            elif formato == None:
                videos = Video.objects.all()  # lista de todos los videos que tengo en la bbdd
                if len(videos) != 0:
                    top_10 = orden_videos(videos)
                    videos_mainpage = lista_videos(top_10)
                    alimentadores = Channel.objects.all().filter(selected=True)
                    total_votes(alimentadores)
                    if request.user.is_authenticated:
                        username = request.user.username
                        user = User.objects.get(username=username)
                        votos = Votes.objects.all().filter(user=user)
                        if len(votos) != 0:
                            last_voted = last_voted_videos(votos)
                            list_last_voted = lista_videos(last_voted)
                        else:
                            list_last_voted = []
                        for video in videos:
                            try:
                                vote = Votes.objects.get(videoId=video, user=user)
                            except Votes.DoesNotExist:
                                vote = Votes(videoId=video, user=user)
                                vote.save()

                        context = {'videos': videos_mainpage, 'alimentadores': alimentadores, 'votos': votos,
                                   'list_last_voted': list_last_voted}
                        template = get_template('miscosas/index_extension.html')
                    else:
                        context = {'videos': videos_mainpage, 'alimentadores': alimentadores}
                        template = get_template('miscosas/index_extension.html')
                    return HttpResponse(template.render(context, request))
                else:
                    context = {}
                    template = get_template('miscosas/index_extension.html')
                    return HttpResponse(template.render(context, request))


# PÁGINA ALIMENTADORES
def todos_alimentadores(request):
    alimentadores = Channel.objects.all()
    context = {'lista': alimentadores}
    template = get_template('miscosas/plantilla_alimentadores.html')
    return HttpResponse(template.render(context, request))


# PÁGINA ALIMENTADOR
def pag_alimentador(request, id):
    try:
        alimentador = Channel.objects.get(channelId=id)
    except Channel.DoesNotExist:
        alimentador = None
    videos = Video.objects.all().filter(channel=alimentador)
    context = {'alimentador': alimentador, 'videos': videos}
    template = get_template('miscosas/plantilla_alimentador.html')
    return HttpResponse(template.render(context, request))


# PÁGINA ÍTEM
@xframe_options_exempt
def pag_item(request, id, videoId):
    if request.user.is_authenticated:
        username = request.user.username
        user = User.objects.get(username=username)
        item = Video.objects.get(id=videoId)
        comentarios = Commentary.objects.all().filter(videoId=item)
        try:
            vote = Votes.objects.get(videoId=item, user=user)
        except Votes.DoesNotExist:
            vote = Votes(videoId=item, user=user)
            vote.save()

        context = {'item': item, 'comentarios': comentarios, 'vote': vote}
        template = get_template('miscosas/plantilla_item.html')
        return HttpResponse(template.render(context, request))
    else:
        try:
            item = Video.objects.get(id=videoId)
        except Video.DoesNotExist:
            item = Video(id=videoId)
        comentarios = Commentary.objects.all().filter(videoId=item)
        context = {'item': item, 'comentarios': comentarios}
        template = get_template('miscosas/plantilla_item.html')
        return HttpResponse(template.render(context, request))


# AÑADIR COMENTARIO AL ÍTEM
def addcomment(request, id, videoId):
    if request.user.is_authenticated:
        item = Video.objects.get(id=videoId)
        username = request.user.username
        user = User.objects.get(username=username)
        texto = request.POST.get('comentario')
        new_com = Commentary(user=user, videoId=item, texto=texto)
        new_com.save()

        return HttpResponseRedirect('/alimentadores/' + str(id) + '/' + str(videoId))
    else:
        return HttpResponseRedirect('/alimentadores/' + str(id) + '/' + str(videoId))


# AÑADIR VOTO AL ÍTEM
def addvote(request, id, videoId):
    if request.user.is_authenticated:
        item = Video.objects.get(id=videoId)
        alimentadores = Channel.objects.all().filter(channelId=id)

        # username = request.user.username
        # user = User.objects.get(username=username)

        user = request.user
        voto_post = request.POST.get('voto')

        voto = Votes.objects.get(user=user, videoId=videoId)
        if voto_post == "Like":
            if voto.vote == 0:
                voto.vote = 1
                voto.save()
                item.positive_votes = item.positive_votes + 1
                item.save()
            elif voto.vote == 1:
                pass
            elif voto.vote == -1:
                voto.vote = 1
                voto.save()
                item.positive_votes = item.positive_votes + 1
                item.negative_votes = item.negative_votes - 1
                item.save()

        elif voto_post == "Dislike":
            if voto.vote == 0:
                voto.vote = -1
                voto.save()
                item.negative_votes = item.negative_votes + 1
                item.save()
            elif voto.vote == -1:
                pass
            elif voto.vote == 1:
                voto.vote = -1
                voto.save()
                item.positive_votes = item.positive_votes - 1
                item.negative_votes = item.negative_votes + 1
                item.save()
        total_votes(alimentadores)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

# LOGIN
@csrf_protect
def logIn(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
        else:
            pass
    return HttpResponseRedirect('/')

# LOGOUT
@csrf_protect
def logOut(request):
    logout(request)
    return HttpResponseRedirect('/')

# REGISTRO DE USUARIOS
@csrf_exempt
def register(request):
    if request.method == "GET":
        context = {}
        template = get_template('miscosas/plantilla_registro.html')
        return HttpResponse(template.render(context, request))
    elif request.method == "POST":
        username = request.POST.get('username')
        password = make_password(request.POST.get('password'))

        user = User(username=username, password=password)
        user.is_staff = True
        user.save()

        profile = UserProfile(user=user, avatar='images/default.jpeg')
        profile.save()

        config = Configuration(user=user)
        config.save()

        return HttpResponseRedirect('/')
    else:
        return HttpResponse("No funciona", status=404)


# sacar los votos asociados al usuario
def num_voted_videos(user):
    voted_pos = Votes.objects.all().filter(user=user, vote=1).count()
    voted_neg = Votes.objects.all().filter(user=user, vote=-1).count()
    num_voted = voted_pos + voted_neg

    #print(num_voted)
    return num_voted

# sacar los comentarios asociados al usuario
def num_commented_videos(user):
    commented = Commentary.objects.all().filter(user=user)
    num_commented = commented.count()

    return num_commented

# PÁGINA DE USUARIOS
@csrf_exempt
def all_users(request):
    users = UserProfile.objects.all()
    num_votes = []
    dic_votes = {}
    num_comments = []
    dic_comments = {}
    for user in users:
        user = user.user
        username = user.username

        num_voted = num_voted_videos(user)
        num_votes.append(num_voted)
        dic_votes[username] = num_voted

        num_commented = num_commented_videos(user)
        num_comments.append(num_commented)
        dic_comments[username] = num_commented

    context = {'users': users, 'dic_votes': dic_votes, 'dic_comments': dic_comments}
    template = get_template('miscosas/plantilla_usuarios.html')
    return HttpResponse(template.render(context, request))

# FUNCIÓN PARA MODIFICAR ESITLO Y LETRA PARA USUARIO
def css(request):
    color = "#F7EFEF"
    size = 13
    if request.user.is_authenticated:
        username = request.user.username
        user = User.objects.get(username=username) #objeto usuario
        user_config = Configuration.objects.get(user=user)
        color = user_config.color
        size = user_config.size
    context = {"color": color, "size": size}
    template = get_template("miscosas/css/html.css")
    return HttpResponse(template.render(context, request), content_type="text/css")

def css_id(request, id):
    color = "#F7EFEF"
    size = 13
    if request.user.is_authenticated:
        username = request.user.username
        user = User.objects.get(username=username) #objeto usuario
        user_config = Configuration.objects.get(user=user)
        color = user_config.color
        size = user_config.size
    context = {"color": color, "size": size}
    template = get_template("miscosas/css/html.css")
    return HttpResponse(template.render(context, request), content_type="text/css")

# PÁGINA USUARIO
def pag_user(request, name):
    if request.method == 'GET':
        try:
            user_page = User.objects.get(username=name)
        except User.DoesNotExist:
            user_page = User(username=name)
        commented_videos = Commentary.objects.all().filter(user=user_page)
        voted_videos = Votes.objects.all().filter(user=user_page)
        if request.user.is_authenticated:
            username = request.user.username
            user = User.objects.get(username=username)
            context = {}
            context['commented_videos'] = commented_videos
            context['voted_videos'] = voted_videos
            # videos votados videos comentados
            if username == name:
                photo_upload_form = ProfileForm(request.POST, request.FILES)
                context['photo_upload_form'] = photo_upload_form
                if photo_upload_form.is_valid():
                    avatar = photo_upload_form.cleaned_data.get('profile_photo')
                    try:
                        new_user_profile = UserProfile.objects.get(user=user)
                        new_user_profile.avatar = avatar
                        new_user_profile.save()
                    except UserProfile.DoesNotExist:
                        new_user_profile = UserProfile(user=user, avatar=avatar)
                        new_user_profile.save()
                avatar = UserProfile.objects.get(user=user_page)
                context['avatar'] = avatar

                return render(request, 'miscosas/plantilla_usuario.html', context)
            else:
                avatar = UserProfile.objects.get(user=user_page)
                context['avatar'] = avatar
                template = get_template('miscosas/plantilla_usuario.html')
                return HttpResponse(template.render(context, request))
        else:
            try:
                user_page = User.objects.get(username=name)
            except User.DoesNotExist:
                user_page = User(username=name)
            commented_videos = Commentary.objects.all().filter(user=user_page)
            voted_videos = Votes.objects.all().filter(user=user_page)
            #videos comentados y videos votados
            try:
                avatar = UserProfile.objects.get(user=user_page)
            except UserProfile.DoesNotExist:
                avatar = UserProfile(user=user_page, avatar='images/default.jpeg')
            context = {'avatar': avatar}
            context['commented_videos'] = commented_videos
            context['voted_videos'] = voted_videos
            template = get_template('miscosas/plantilla_usuario.html')
            return HttpResponse(template.render(context, request))
    elif request.method == 'POST':
        new_style = request.POST.get("Style")
        new_size = request.POST.get("Size")
        try:
            user = User.objects.get(username=name)
        except User.DoesNotExist:
            user = User(username=name)
        try:
            new_config = Configuration.objects.get(user=user)
        except Configuration.DoesNotExist:
            new_config = Configuration(user=user)
        if new_style != None and new_style != new_config.color:
            new_config.color = new_style
            new_config.save()
        if new_size != None and new_size != new_config.size:
            new_config.size = new_size
            new_config.save()
        try:
            user = User.objects.get(username=name)
        except User.DoesNotExist:
            user = User(username=name)
        try:
            user_config = Configuration.objects.get(user=user)
        except Configuration.DoesNotExist:
            user_config = Configuration(user=user)
        context = {'user': user, 'user_config': user_config}

        commented_videos = Commentary.objects.all().filter(user=user)
        voted_videos = Votes.objects.all().filter(user=user)
        if request.user.is_authenticated:
            username = request.user.username
            user = User.objects.get(username=username)
            context['commented_videos'] = commented_videos
            context['voted_videos'] = voted_videos
            # videos votados videos comentados
            if username == name:
                photo_upload_form = ProfileForm(request.POST, request.FILES)
                context['photo_upload_form'] = photo_upload_form
                if photo_upload_form.is_valid():
                    avatar = photo_upload_form.cleaned_data.get('profile_photo')
                    try:
                        new_user_profile = UserProfile.objects.get(user=user)
                        new_user_profile.avatar = avatar
                        new_user_profile.save()
                    except UserProfile.DoesNotExist:
                        new_user_profile = UserProfile(user=user, avatar=avatar)
                        new_user_profile.save()
                avatar = UserProfile.objects.get(user=user)
                context['avatar'] = avatar

                return render(request, 'miscosas/plantilla_usuario.html', context)
            else:
                avatar = UserProfile.objects.get(user=user)
                context['avatar'] = avatar
                template = get_template('miscosas/plantilla_usuario.html')
                return HttpResponse(template.render(context, request))
        else:
            try:
                user_page = User.objects.get(username=name)
            except User.DoesNotExist:
                user_page = User(username=name)
            # videos comentados y videos votados
            try:
                avatar = UserProfile.objects.get(user=user_page)
            except UserProfile.DoesNotExist:
                avatar = UserProfile(user=user_page, avatar='images/default.jpeg')
            context = {'avatar': avatar}
            context['commented_videos'] = commented_videos
            context['voted_videos'] = voted_videos
            template = get_template('miscosas/plantilla_usuario.html')
            return HttpResponse(template.render(context, request))

# SELECCIONAR/DESELECCIONAR ALIMENTADOR
@csrf_exempt
def deselect(request, id):
    try:
        alimentador = Channel.objects.get(channelId=id)
        alimentador.selected = False
        alimentador.save()
    except Channel.DoesNotExist:
        alimentador = None

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@csrf_exempt
def intermediate(request):
    id_channel = request.POST['channelId']

    return HttpResponseRedirect('select/' + str(id_channel))

@csrf_exempt
def select(request, id):
    try:
        alimentador = Channel.objects.get(channelId=id)
    except Channel.DoesNotExist:
        alimentador = Channel(channelId=id)

    alimentador.selected = True
    alimentador.save()

    try:
        channelDic = parse(id)
    except urllib.error.HTTPError:
        channelDic = parse_wk(id)

    loadData(channelDic, id)

    return HttpResponseRedirect('/alimentadores/' + str(id))

# PÁGINA DE INFORMACIÓN
@csrf_exempt
def pag_info(request):
    context = {}
    template = get_template('miscosas/plantilla_informacion.html')
    return HttpResponse(template.render(context, request))

# OPCIONAL: COMENTARIOS EN FORMATO XML
def comentarios_xml(request):
    if request.method == "GET":
        comentarios = Commentary.objects.all()
        context = {'comentarios': comentarios}
        template = get_template('miscosas/plantilla_comentariosxml.xml')
        return HttpResponse(template.render(context, request), content_type='text/xml')

# OPCIONAL: COMENTARIOS EN FORMATO JSON
def comentarios_json(request):
    dict_comentarios = {}
    if request.method == 'GET':
        comentarios = Commentary.objects.all()
        for comentario in comentarios:
            dict_comentarios[comentario.videoId.title] = {
                'comentario': comentario.texto,
                'usuario': comentario.user.username,
                'fecha': comentario.fecha,
                'enlaceitem': '/alimentadores/' + comentario.videoId.channel.channelId + '/' + comentario.videoId.id,
            }

    #print(json.dumps(dict_comentarios, default=str, indent=4))
    return JsonResponse(dict_comentarios)