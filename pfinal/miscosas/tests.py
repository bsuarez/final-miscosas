from django.test import SimpleTestCase, TestCase
from . import views


class TestHTTP(TestCase):

    def setUp(self):
        self.id_channel = 'UC300utwSVAYOoRLEqmsprfg'
        self.id_item = 'zEpwMRhczd0'
        self.user = 'ana'
        self.psswd = 'ana'

    def test_main_ok(self):
        response = self.client.get('/')
        self.assertEqual(response.resolver_match.func, views.main_page)
        self.assertInHTML("<title>Mis Cosas</title>", response.content.decode(encoding='UTF-8'))

    def test_main_xml(self):
        response = self.client.get('/?format=xml')
        self.assertEqual(response.status_code, 200)

    def test_login_ok(self):
        response = self.client.post('/logIn', {'username': self.user, 'password': self.psswd})
        self.assertEqual(response.status_code, 302)

    def test_logout_ok(self):
        response = self.client.post('/logOut', {'username': self.user, 'password': self.psswd})
        self.assertEqual(response.status_code, 302)

    def test_register_ok(self):
        response = self.client.get('/register')
        self.assertEqual(response.status_code, 200)
        self.assertInHTML("<h1><u>Regístrate en la página</u></h1>", response.content.decode(encoding='UTF-8'))

    def test_register_post(self):
        response = self.client.post('/register', {'username': 'pepe', 'password': 'pepe'})
        self.assertEqual(response.status_code, 302)

    def test_users_ok(self):
        response = self.client.get('/users')
        self.assertEqual(response.status_code, 200)
        self.assertInHTML("<h1><u>Todos los usuarios</u></h1>", response.content.decode(encoding='UTF-8'))

    def test_user_ok(self):
        response = self.client.get('/users/' + self.user)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.pag_user)
        self.assertInHTML("<h3><u><strong>Ítems votados:</strong></u></h3>", response.content.decode(encoding='UTF-8'))

    def test_user_post(self):
        response = self.client.post('/users/' + self.user)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.pag_user)
        self.assertInHTML("<h3><u><strong>Ítems comentados:</strong></u></h3>",
                          response.content.decode(encoding='UTF-8'))

    def test_about_ok(self):
        response = self.client.get('/about')
        self.assertEqual(response.resolver_match.func, views.pag_info)
        self.assertEqual(response.status_code, 200)
        self.assertInHTML(" <h3><strong>Autora: </strong></h3>", response.content.decode(encoding='UTF-8'))

    def test_addcomment_ok(self):
        response = self.client.post('/addcomment/' + self.id_channel + '/' + self.id_item)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.addcomment)

    def test_addvote_ok(self):
        response = self.client.post('/addvote/' + self.id_channel + '/' + self.id_item)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.addvote)

    def test_select_ok(self):
        response = self.client.post('/select/' + self.id_channel)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.select)

    def test_deselect_ok(self):
        response = self.client.post('/deselect/' + self.id_channel)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.deselect)

    def test_alimentadores_ok(self):
        response = self.client.get('/alimentadores')
        self.assertEqual(response.status_code, 200)
        self.assertInHTML("<h1><u>Alimentadores disponibles</u></h1>", response.content.decode(encoding='UTF-8'))

    def test_alimentador_ok(self):
        response = self.client.get('/alimentadores/' + self.id_channel)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.pag_alimentador)

    def test_item_ok(self):
        response = self.client.get('/alimentadores/' + self.id_channel + '/' + self.id_item)
        self.assertEqual(response.status_code, 200)
        self.assertInHTML("<h4><strong>Información del alimentador: </strong></h4>",
                          response.content.decode(encoding='UTF-8'))

    def test_comentarios_xml(self):
        response = self.client.get('/comentarios/xml')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.comentarios_xml)

    def test_comentarios_json(self):
        response = self.client.get('/comentarios/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.comentarios_json)
