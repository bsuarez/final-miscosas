# Generated by Django 3.0.3 on 2020-05-26 18:18

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('miscosas', '0008_auto_20200524_1653'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='video',
            name='commentary',
        ),
        migrations.CreateModel(
            name='Comentary',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', models.TextField(max_length=256)),
                ('fecha', models.DateField(auto_now=True)),
                ('user', models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('videoId', models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='miscosas.Video')),
            ],
        ),
    ]
