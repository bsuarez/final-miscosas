# Generated by Django 3.0.3 on 2020-05-24 11:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('miscosas', '0004_seleccionados'),
    ]

    operations = [
        migrations.AddField(
            model_name='seleccionados',
            name='esta_seleccionado',
            field=models.CharField(default='NO', max_length=256),
        ),
    ]
