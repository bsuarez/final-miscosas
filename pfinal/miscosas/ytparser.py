#!/usr/bin/python3

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from urllib.request import urlopen
import sys
import os.path
import string


class YTHandler(ContentHandler):
    """Class to handle events fired by the SAX parser
    Fills in self.videos with title, link and id for videos
    in a YT channel XML document.
    """

    def __init__(self):
        """Initialization of variables for the parser
        * inEntry: within <entry>
        * inContent: reading interesting target content (leaf strings)
        * content: target content being readed
        * title: title of the current entry
        * id: id of the current entry
        * link: link of the current entry
        * videos: list of videos (<entry> elements) in the channel,
            each video is a dictionary (title, link, id)
        """
        self.inEntry = False
        self.inContent = False
        self.inChannelTitle = True
        self.inChannelLink = True

        # CHANNEL
        self.content = ""
        self.channelTitle = ""
        self.channelLink = ""

        #VIDEO
        self.id = ""
        self.link = ""
        self.title = ""
        self.videoDescrip = ""
        self.videoRating = ""
        self.videoStatistic = ""
        self.videos = [] #lista de diccionarios con los videos

        self.dic = {} #me guardo toda la info anterior: canal relacionado con videos

    def startElement(self, name, attrs):
        if name == 'title':
            self.inContent = True
        if name == 'link':
            if self.inChannelLink:
                if attrs.get('rel') == 'alternate':
                    self.channelLink = attrs.get('href')
                    self.inChannelLink = False

        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'yt:videoId':
                self.inContent = True
            elif name == 'media:description':
                self.inContent = True

    def endElement(self, name):
        global videos

        #print(name)
        if self.inChannelTitle:
            if name == 'title':
                self.channelTitle = self.content
                self.content = ""
                self.inContent = False
                self.inChannelTitle = False

        if name == 'entry':
            self.inEntry = False
            self.videos.append({'link': self.link,
                                'title': self.title,
                                'id': self.id,
                                'description': self.videoDescrip})
        elif self.inEntry:
            if name == 'title':
                self.title = self.content
                self.content = ""
                self.inContent = False
            elif name == 'yt:videoId':
                self.id = self.content
                self.content = ""
                self.inContent = False
            elif name == 'media:description':
                self.videoDescrip = self.content
                self.content = ""
                self.inContent = False

    def characters(self, chars):
        if self.inContent:
            self.content = self.content + chars

    def getLista(self):
        return self.videos

    def getChannelTitle(self):
        return self.channelTitle

    def getChannelLink(self):
        return self.channelLink

    def getDic(self):
        self.dic['channelTitle'] = self.channelTitle
        self.dic['channelLink'] = self.channelLink
        self.dic['numVideos'] = len(self.videos)
        self.dic['videos'] = self.videos
        return(self.dic)



# theParser = make_parser()
# theHandler = YTHandler()
# theParser.setContentHandler(theHandler)
#
# # Ready, set, go!
# xmlFile = urlopen('https://www.youtube.com/feeds/videos.xml?channel_id=UCT7Sl7NqhFT7eLLCCjIYwRg')
# theParser.parse(xmlFile)
# #
# # lista = theHandler.getLista()
# # print(lista)
# #
# dic = theHandler.getDic()
# print(dic)

# title = theHandler.getChannelTitle()
# print("Título canal: " + title)
#
# link = theHandler.getChannelLink()
# print("Link del canal:" + link)
#
# videos = theHandler.getnumVideos()
# print("Número de vídeos: " + str(videos))

#print ("Parse complete")